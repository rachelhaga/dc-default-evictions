get_geocode_df <- function(address){
  
  assertthat::assert_that(
    Sys.getenv("GGMAP_GOOGLE_API_KEY") != "",
    msg = "Your google maps api key isn't correctly set"
  )
  
  geocode <- googleway::google_geocode(
    address = stringr::str_replace_all(address, "#", "%23"), 
    key = Sys.getenv("GGMAP_GOOGLE_API_KEY")
  )
  
  if(geocode$status == "OK"){
    
    geocode_df <- data.frame(
      address = address,
      lat = geocode$results$geometry$location$lat,
      lon = geocode$results$geometry$location$lng,
      frmt_address = geocode$results$formatted_address,
      address_type = geocode$results$geometry$location_type,
      type = paste(geocode$results$types),
      stringsAsFactors = FALSE
    ) %>%
      filter(type != "intersection")
    
  } else {
    
    geocode_df <- data.frame(
      address = address,
      lat = NA,
      lon = NA,
      frmt_address = NA,
      address_type = NA,
      type = NA,
      stringsAsFactors = FALSE
    )
    
  }  
  
  return(geocode_df)    
}

merge_geocodes <- function(defaults){
  
  # get gecode coords pulled from google maps using address
  address_path <- "data/addresses_geocode.rds"
  
  if(file.exists(address_path)){
    
    address_df <- read_rds(address_path) %>%
      group_by(address) %>%
      slice(1) %>%
      ungroup()
    
  } else {
    
    address_df <- defaults %>%
      distinct(address) %>%
      pull(address) %>%
      purrr::map_dfr(~get_geocode_df(.x))
    
  }

  # add to defaults df
  defaults <- defaults %>%
    left_join(address_df, by = "address") %>%
    st_as_sf(coords = c('lon', 'lat'), crs = 4269, remove = FALSE)
  
  return(defaults)
  
}

get_travel_df <- function(origin, destination){
  
  assertthat::assert_that(
    Sys.getenv("GGMAP_GOOGLE_API_KEY") != "",
    msg = "Your google maps api key isn't correctly set"
  )
  
  car_dist <- google_distance(
    origins = str_replace_all(origin, "#", "%23"),
    destinations = str_replace_all(destination, "#", "%23"),
    mode = "driving",
    key = Sys.getenv("GGMAP_GOOGLE_API_KEY")
  )
  
  walk_dist <- google_distance(
    origins = str_replace_all(origin, "#", "%23"),
    destinations = str_replace_all(destination, "#", "%23"),
    mode = "walking",
    key = Sys.getenv("GGMAP_GOOGLE_API_KEY")
  )
  
  if(car_dist$status == "OK" & walk_dist$status == "OK"){
    
    travel_df <- data.frame(
        address = destination,
        preceding_address = origin,
        drive_dist_m = car_dist$rows$elements[[1]]$distance$value,
        walk_dist_m = walk_dist$rows$elements[[1]]$distance$value,
        drive_time_s = car_dist$rows$elements[[1]]$duration$value,
        walk_time_s = walk_dist$rows$elements[[1]]$duration$value,
        stringsAsFactors = FALSE
      ) %>% 
      mutate(
        drive_dist_mi = conv_unit(drive_dist_m, "m", "mile"),
        walk_dist_mi = conv_unit(walk_dist_m, "m", "mile"),
        dist_mi = if_else(drive_dist_mi <= 0.2, walk_dist_mi, drive_dist_mi),
        time_s = if_else(drive_dist_mi <= 0.2, walk_time_s, drive_time_s)
      )
    
  } else {
    
    travel_df <- data.frame(
      address = destination,
      preceding_address = origin,
      drive_dist_m = NA,
      walk_dist_m = NA,
      drive_time_s = NA,
      walk_time_s = NA,
      drive_dist_mi = NA,
      walk_dist_mi = NA,
      dist_mi = NA,
      time_s = NA,
      stringsAsFactors = FALSE
    )
    
  }
  
  return(travel_df)
  
}


merge_travel_times <- function(trips){
  
  # get travel times and dist pulled from google maps using address
  travel_path <- "data/trip_distance.rds"
  
  if(file.exists(travel_path)){
    
    travel_df <- read_rds(travel_path) %>%
      group_by(preceding_address, address, dist_mi, time_s)
    
  } else {
  
    travel_df <- trips %>%
      drop_na(preceding_address) %>%
      select(preceding_address, address) %>%
      rename(
        origin = preceding_address,
        destination = address
      ) %>%
      pmap_dfr(get_travel_df)
    
  }
  
  trips <- trips %>%
    left_join(travel_df, by = c("address", "preceding_address"))
  
  return(trips)
}

