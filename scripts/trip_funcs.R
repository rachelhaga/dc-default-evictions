defaults_to_trips <- function(defaults){
  
  attempts_recode = list(
    "first_attempt_datetime" = "1",
    "second_attempt_datetime" = "2",
    "third_attempt_datetime" = "3"
  )
  
  trips <- defaults %>%
    pivot_longer(
      cols = c(first_attempt_datetime, second_attempt_datetime, third_attempt_datetime),
      names_to = "attempt",
      values_to = "attempt_datetime"
    ) %>%
    mutate(attempt = recode(attempt, !!!attempts_recode)) %>%
    select(-contains("_attempt_")) %>%
    drop_na(attempt_datetime) %>%
    mutate(attempt_date = as_date(floor_date(attempt_datetime, "day"))) %>%
    group_by(server_id) %>%
    arrange(attempt_datetime, .by_group = TRUE) %>%
    mutate(
      preceding_datetime = lag(attempt_datetime),
      preceding_address = lag(address)
    ) %>%
    ungroup() %>%
    mutate(travel_duration = attempt_datetime - preceding_datetime)
  
  return(trips)
}

#' Function: get_trip_dist
#' 
#' Count the number trips each server took on a given date
#' Impute missing dates + servers combinations with `0`
#' Limit to process servers with at least 20 defendants of the time

get_trip_dist <- function(defaults){
  
  trips <- defaults_to_trips(defaults)
  
  trips_dist <- trips %>%
    filter(!is.na(server_grp)) %>%
    mutate(server_id = fct_drop(server_id)) %>%
    group_by(server_id, attempt_date) %>%
    summarize(n = n_distinct(address)) %>%
    ungroup() %>%
    complete(attempt_date = full_seq(attempt_date, 1), server_id, fill = list(n = 0)) %>%
    arrange(server_id, attempt_date) %>%
    group_by(server_id) %>%
    mutate(
      n_cumsum = cumsum(n),
      n_cumsum_pct = cumsum(n)/sum(n)
    ) %>%
    ungroup()
  
  return(trips_dist)
  
}

#' Function: calculate_trip_dist_ks_test
#' 
#' Compare the distributions with a KS test
#' How similar are the attempts dates over time for different process servers

calculate_trip_dist_ks_test <- function(trips_dist){
  
  trips_ks <- trips_dist %>%
    select(-n, -n_cumsum) %>%
    pivot_wider(names_from = server_id, values_from = n_cumsum_pct) %>%
    select(-attempt_date)
  
  trip_ks_test <- expand_grid(
      dist1 = colnames(trips_ks), 
      dist2 = colnames(trips_ks)
    ) %>%
    mutate(
      test = map2(
        .x = dist1, 
        .y = dist2, 
        ~ks.test(trips_ks[[.x]], trips_ks[[.y]])
      ),
      tidied = map(test, broom::tidy)
    ) %>%
    select(-test) %>%
    unnest(tidied)
  
  return(trip_ks_test)
  
}

#'
#'
#'
#'
get_trip_ks_lower_tri <- function(trip_ks){
  
  trip_tri <- trip_ks %>%
    select(dist1, dist2, p.value) %>%
    pivot_wider(names_from = dist2, values_from = p.value) %>%
    column_to_rownames("dist1") %>%
    grab_lower_tri() %>%
    rownames_to_column("dist1") %>%
    pivot_longer(-dist1, names_to = "dist2", values_to = "p_value") %>%
    drop_na()
  
  return(trip_tri)
  
}
